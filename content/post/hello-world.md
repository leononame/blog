---
title: "Hello World"
date: 2019-08-04T17:25:57+07:00
lastmod: 2019-08-13
draft: false
slug: hello
---

{{< highlight go "linenos=table,hl_lines=8" >}}
package main

import (
	"fmt"
)

// print Hello, world
func main() {
	fmt.Println("Hello, world")
}
{{< / highlight >}}
