+++
date = "2019-08-04"
description = "About me"
title = "About"
+++

Welcome to my personal blog. My name is Leo and this is where I write about stuff that fascinates me.

I will share stuff that I learn about, things that I'm currently interested in. I try to write the articles I wish I found when looking for information.

I work as a Backend Developer. When I don't code I read. I enjoy music, walking, meeting my friends. I try to be honest with myself at all times.
