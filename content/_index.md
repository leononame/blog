+++
date = "2019-08-04"
title = "Home"
+++
Hi, my name is Leo. I like machines, code, books and music. This is my personal space and I write about stuff that interests me. [Click here]({{< ref "about.md" >}}) for more about me or start reading.
