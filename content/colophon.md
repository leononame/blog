+++
date = "2019-10-15"
title = "Colophon"
draft = false
+++

This page contains the technical details about my blog. 

This blog is written using [Hugo](https://gohugo.io/) as a static site generator. The source code for the blog can be found [here](https://gitlab.com/leononame/blog/). I wrote the theme myself because I wanted a nice-looking, responsive blog without any JS. You can inspect the source code of the theme in the aforementioned git repo. If you see any display errors, I would appreciate if you sent me an [email](mailto:contact@leononame.dev) or contact me on [Twitter](https://twitter.com/leononame2).

The site is built on Gitlab CI and minified before being deployed on a [Hetzner Online](https://www.hetzner.com/cloud) VPS. The server runs [Debian](https://www.debian.org/) and the content is served through [nginx](https://nginx.org/).